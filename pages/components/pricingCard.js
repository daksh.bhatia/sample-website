import React from "react";

export default class PricingCard extends React.Component {
  render() {
    return (
      <div className="col">
        <div
          className={
            "card mb-4 rounded-3 shadow-sm " +
            (this.props.border == true ? "border-primary" : "")
          }
        >
          <div
            className={
              "card-header py-3 " +
              (this.props.titleFill == true ? "bg-primary text-white" : "")
            }
          >
            <h4 className="my-0 fw-normal">{this.props.name}</h4>
          </div>
          <div className="card-body">
            <h1 className="card-title pricing-card-title">
              ${this.props.price}
              <small className="text-muted fw-light">/mo</small>
            </h1>
            <ul className="list-unstyled mt-3 mb-4">
              {this.props.features?.map((item, i) => (
                <li key={i}>{item}</li>
              ))}
            </ul>
            <button
              type="button"
              className={
                "w-100 btn btn-lg " +
                (this.props.buttonFill == true
                  ? "btn-primary"
                  : "btn-outline-primary ")
              }
            >
              {this.props.buttonText}
            </button>
          </div>
        </div>
      </div>
    );
  }
}

import React from "react";
import Head from "next/head";
import Header from "./components/header";
import Footer from "./components/footer";

class About extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <Head>
          <title>About Us</title>
        </Head>
        <div className="container py-3 " style={{ maxWidth: 960 }}>
          <Header />
          <div>
            <h2 className="text-center">Hello World !!</h2>
            <p></p>
          </div>
          <Footer />
        </div>
      </div>
    );
  }
}

export default About;

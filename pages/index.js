import React from "react";
import Head from "next/head";
import Header from "./components/header";
import Footer from "./components/footer";
import PricingCard from "./components/pricingCard";
import Table from "./components/table";

export default class Home extends React.Component {
  render() {
    return (
      <div>
        <Head>
          <title>Pricing Example - Bootstrap v5.0</title>
        </Head>
        <div className="container py-3 " style={{ maxWidth: 960 }}>
          <Header />
          <div
            className="pricing-header p-3 pb-md-4 mx-auto text-center"
            style={{ maxWidth: 700 }}
          >
            <h1 className="display-4 fw-normal">Pricing</h1>
            <p className="fs-5 text-muted">
              Quickly build an effective pricing table for your potential
              customers with this Bootstrap example. It’s built with default
              Bootstrap components and utilities with little customization.
            </p>
          </div>

          <div className="row row-cols-1 row-cols-md-3 mb-3 text-center">
            <PricingCard
              name="Free"
              price={0}
              features={[
                "10 users included",
                "2 GB of storage",
                "Email support",
                "Help center access",
              ]}
              buttonText="Sign up for free"
            />
            <PricingCard
              name="Pro"
              price={15}
              features={[
                "20 users included",
                "10 GB of storage",
                "Priority email support",
                "Help center access",
              ]}
              buttonText="Get started"
              buttonFill={true}
            />
            <PricingCard
              name="Enterprise"
              price={29}
              features={[
                "30 users included",
                "15 GB of storage",
                "Phone and email support",
                "Help center access",
              ]}
              buttonText="Contact us"
              border={true}
              buttonFill={true}
              titleFill={true}
            />
          </div>

          <h2 className="display-6 text-center mb-4">Compare plans</h2>
          <Table />
          <Footer />
        </div>
      </div>
    );
  }
}

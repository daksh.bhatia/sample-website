import React from "react";
import { TiTick } from "react-icons/ti";

export default class Table extends React.Component {
  render() {
    return (
      <div className="table-responsive">
        <table className="table text-center">
          <thead>
            <tr>
              <th style={{ width: "34%" }}></th>
              <th style={{ width: "22%" }}>Free</th>
              <th style={{ width: "22%" }}>Pro</th>
              <th style={{ width: "22%" }}>Enterprise</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row" className="text-start">
                Public
              </th>
              <td>
                {" "}
                <TiTick />{" "}
              </td>
              <td>
                {" "}
                <TiTick />
              </td>
              <td>
                {" "}
                <TiTick />
              </td>
            </tr>
            <tr>
              <th scope="row" className="text-start">
                Private
              </th>
              <td></td>
              <td>
                {" "}
                <TiTick />{" "}
              </td>
              <td>
                {" "}
                <TiTick />
              </td>
            </tr>
          </tbody>

          <tbody>
            <tr>
              <th scope="row" className="text-start">
                Permissions
              </th>
              <td>
                <TiTick />{" "}
              </td>
              <td>
                {" "}
                <TiTick />
              </td>
              <td>
                {" "}
                <TiTick />
              </td>
            </tr>
            <tr>
              <th scope="row" className="text-start">
                Sharing
              </th>
              <td></td>
              <td>
                {" "}
                <TiTick />
              </td>
              <td>
                {" "}
                <TiTick />
              </td>
            </tr>
            <tr>
              <th scope="row" className="text-start">
                Unlimited members
              </th>
              <td></td>
              <td>
                {" "}
                <TiTick />
              </td>
              <td>
                {" "}
                <TiTick />
              </td>
            </tr>
            <tr>
              <th scope="row" className="text-start">
                Extra security
              </th>
              <td></td>
              <td></td>
              <td>
                {" "}
                <TiTick />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
